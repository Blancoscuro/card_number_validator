extern crate simple_logger;
extern crate log;
extern crate clap;
extern crate card_verification_digit;

use clap::Parser;
use simple_logger::SimpleLogger;
use card_verification_digit::{
    Pan, PanWithoutVerificationDigits
};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Input {
    /// The string to be processed
    value: String,
    // Indicates if the verification digit should be generated based on the provided string
    #[arg(short)]
    generate: bool
}

#[derive(Debug)]
enum Error {
    InvalidLength, InvalidInput
}

fn main()-> Result<(), Error> {
    SimpleLogger::new().init().unwrap();
    let input = Input::parse();

    let digits = input.value.chars()
        .map(|c|{c.to_digit(10)})
        .map(|c|{c.map_or(Err(Error::InvalidInput), |val| { Ok(val) })})
        .collect::<Result<Vec<u32>,Error>>()?;

    if input.generate {
        log::info!("Generating PAN.");
        let generated_pan = PanWithoutVerificationDigits::new(&digits[..])
            .map(|d| { d.generate_verification_pan() })
            .map_err(|_e| {Error::InvalidLength })?;

        println!("New PAN generated.");
        println!("PAN: {:?}", generated_pan.to_value());

    } else {
        let is_valid = Pan::new(&digits[..])
            .map(|d| {d.is_valid()})
            .map_err(|_e| {Error::InvalidLength})?;

        if is_valid { println!("The PAN {:?} is valid.", digits) } else { println!("The PAN {:?} in not valid", digits) }
    }

    Ok(())
}
